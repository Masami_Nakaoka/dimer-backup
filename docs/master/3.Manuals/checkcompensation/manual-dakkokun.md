---
permalink: manual-dakkokun
category: check compensation
title: 管理者向け｜だっこくんの使い方
---



### 管理者向け｜だっこくんの使い方

#### ログイン方法

1. 旧管理画面のだっこくんにある「Move to Dakkokun's Room」をクリック  

   ![Move to Dakkokun's Room](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/be3a17d8f60b6ceaa90331bd2e56f24adefffbb1/assets/img/manual/checkcompensation/dakkokun/MovetoDakkokunsRoom.png)

2. 旧管理画面のIDとPASSでログイン  

   ![Login to Checkcompensation](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/LogintoCheckcompensation.png)

3. ログイン完了  

   ![Login Complete](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/LoginComplete.png)



#### 勤怠の登録方法

1. ログイン後、左のメニューから「Dakkokun's room」をクリック  

   ![Click the link of Dakkokuns room](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClickthelinkDakkokunsroom.png)

2. 「勤怠を登録する」ボタンをクリック  

   ![Click the button to register](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClicktheButtontoRegister.png)

3. 下記の項目を埋める

   - Attendance -> 出勤日時

     - yyyy/mm/dd hh:mmの形式
     - 15分単位でまるめ処理はいってます
       - たとえば。。
         - 2018/05/24 09:58に出勤打刻 -> 2018/05/24 10:00出勤の扱いになる
         - 2018/05/24 10:00に出勤打刻 -> 2018/05/24 10:15出勤の扱いになる

   - Leaving -> 退勤日時

     - yyyy/mm/dd hh:mmの形式
     - または数字の「0」
     - 15分単位でまるめる処理はいってます
       - たとえば。。
         - 2018/05/24 18:50に退勤打刻 -> 2018/05/24 18:45退勤の扱いになる
         - 2018/05/24 19:14に退勤打刻 -> 2018/05/24 19:00退勤の扱いになる

   - Rest -> 休憩時間

     - 0、15、30、45、60、75、90....
     - 15分刻みで入力

   - Place -> 勤務場所

     - 「かいしゃ」または「ざいたく」
     - 平仮名で入力
       - だっこくんは小学1年生の男の子なので、漢字分かりません

   - Message -> メッセージ

     - 備考欄として使ってください

     - 「ここはおとながつかうところだから、かんじでもいいよ」  

       とだっこくんが言ってます

   - User -> ユーザー

     - この勤怠を紐つけるユーザーのフルネームを入力
     - 名字と名前の間は半角スペース
     - 間違えると紐つきません

   入力が終わったら、「Regist」ボタンを押して登録してください



#### 勤怠の修正方法

##### 個別の勤怠表から修正する



1. ログイン後、左のメニューから「Dakkokun's room」をクリック  

   ![Click the link of Dakkokuns room](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClickthelinkDakkokunsroom.png)

2. 画面右上にある「Please select a user」から対象のユーザーを選択

3. 「Show attendance」ボタンをクリック

4. 修正したい勤怠の「Edit」をクリック  

   ![Click the button to Edit](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClicktheButtontoEdit.png)

5. 勤怠を修正

6. 「Submit」をクリック  

   ![Click the button to Submit](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClicktheButtontoSubmit.png)

7. 完了



##### 全員分の勤怠表から修正する



1. ログイン後、左のメニューから「Dakkokun's room」をクリック  

   ![Click the link of Dakkokuns room](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClickthelinkDakkokunsroom.png)

2. 画面右上にある「Please select a user」から「全員分」を選択

3. 「Show attendance」ボタンをクリック

4. 修正したい勤怠の「Edit」をクリック  

   ![Click the button to Edit](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClicktheButtontoEdit2.png)

5. 勤怠を修正

6. 「Submit」をクリック  

   ![Click the button to Submit](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/90afdad99d56073a24f4b2cb90e2816fb412c031/assets/img/manual/checkcompensation/dakkokun/ClicktheButtontoSubmit2.png)

   - ちなみに、「Cancel」のボタンは効きません  

     まだ実装してないので

7. 完了

※ 「Submit」をクリックしても、画面遷移やお知らせの表示はありません  
	まだ実装していないので



## ！Caution！ 画面の仕様は予告なく変更することがあります