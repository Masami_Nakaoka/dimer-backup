---
permalink: rdi-inputmanual
category: receipt data input
title: 入力マニュアル
---

# Input manual

実際の作業についての説明です

!!こちらは現在作成中のドキュメントです!!

## ■作業の流れ

1. 管理画面にログイン
2. 各案件にある「入力開始」のボタンから作業画面に移動
3. 「入力時の注意事項など」に記載されている内容を確認
4. レシートの画像が何枚あるかを確認
5. データ入力をしてもいいレシートの画像かを確認
6. レシートのデータを入力
7. 完了したら次のレシートへ
8. 以降は2 ～ 7の繰り返し

### 管理画面にログイン

ログインしてください  
何も怖くありません 大丈夫です

### 「入力開始」のボタンから作業画面に移動

↓がログイン後の画面  
![TOP screen](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/534b124313df15e878baceadc9388f405e1dcab7/assets/img/manual/receiptdatainput/inputmanual/TOPscreen.png)

「案件一覧」より下部に作業対象の案件が並んでいます

- 優先度が高い（★★★ ＞ ★★ ＞ ★）
- 納期が近い
- 残件数が多い

案件から順番に対応します

「作業開始」のボタンをクリックすると、作業画面へ行けます  
![Work start button](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/ecff1afc5727197808b8e8a38af16d178ee50d92/assets/img/manual/receiptdatainput/inputmanual/workstartbutton.png)

↓が作業画面です  
![Work screen1](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/ecff1afc5727197808b8e8a38af16d178ee50d92/assets/img/manual/receiptdatainput/inputmanual/workscreen1.png)
![Work screen2](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/ecff1afc5727197808b8e8a38af16d178ee50d92/assets/img/manual/receiptdatainput/inputmanual/workscreen2.png)
![Work screen3](https://bitbucket.org/Masami_Nakaoka/dimer-backup/raw/ecff1afc5727197808b8e8a38af16d178ee50d92/assets/img/manual/receiptdatainput/inputmanual/workscreen3.png)


### 「入力時の注意事項など」に記載されている内容を確認

## ■入力をする前に。。。

下記に該当するレシートの画像は**受付出来ませんので。入力も不要です**

1. レシートの4つの角が写っていない画像
2. レシートが途中から折り曲げられている画像
3. レシートが途中から切断されている画像
4. 不鮮明な画像



1 ～ 3に該当する画像は、『レシート全体が写っていない』を選択して処理してください  

4に該当する画像は、『画像が不鮮明』を選択して処理してください



## ■各項目の入力方法

1. チェーン名
   - レシートのチェーン名を入力
   - 英数字記号は半角で入力
   - **「対象のチェーンで買ってません」などのエスカレはしないでください**
2. 店舗名
   - レシートの店舗名を入力
   - 英数字記号は半角で入力
   - 店舗名の記載がない場合はハイフン（-）を入力
   - **「対象の店舗で買ってません」などのエスカレはしないでください**
3. 電話番号
   - 半角数字で入力
   - 数字以外は入力しない
     - 例えば「03-5225-0589」とレシートに印字されていた場合、入力は「0352250589」となる
   - 以下の場合は数字のゼロ（0）だけを入力
     - 電話番号の記載がない
     - 電話番号の記載はあるが、下半分しかなく読み取れない場合
4. 都道府県
   - 電話番号入力後に都道府県の候補が表示される
     - 表示された候補が1つだけの場合
       - なにもしない
     - 2つ以上の候補が表示された場合
       - レシートに店舗住所の記載があれば、その都道府県を選択する
       - レシートに店舗住所がなければ、ハイフン（-）を入力する
5. レシート日時
   - レシートに記載の日時を選択
   - **日付と時刻は分かれて記載されていることがあります！**
     - 必ず、レシートを隅々まで見て日時を確認してください
6. 商品名
   - 対象商品の記載がレシートにあれば、「入力時の注意点など」にあるキーワードを**コピペ**
     - **ミスが多いので、直接入力は禁止です！**
   - 対象商品の記載がない場合は、ハイフン（-）を入力
   - 対象商品を購入しているか判断つかない場合もハイフン（-）を入力
   - **「対象商品を買っていません」や「対象商品か判断出来ません」などのエスカレはしないでください**
7. 税抜額（税込額）
   - **この項目は入力ミスが非常に多いです**
   - **金額のミスはユーザーからのクレームに直結します**
   - 入力時には、下記の順番にチェックのうえ、細心の注意をはらって金額を入力してください
     1. **「入力時の注意点など」で、金額入力のルールを確認**
        - どの商品を何点買う必要があるのか
        - 必要な数以上に買っている場合はどうするのか
        - 必要な数買っていない場合はどうするのか
     2. **対象商品を、必要な数買っているか、金額はいくらかを確認**
        - もし、必要な数買っていない場合は、7.1で確認したルールに沿って対応してください
     3. **小計・合計・対象商品を必要な数買った金額のうち、1番低い金額を確認**
        - 上記3つのうち、1番低い金額が。。。
          1.  小計の場合 -> 小計の金額を入力
          2. 合計の場合 -> 合計の金額を入力
          3. 対象商品を必要な数買った金額の場合 -> 対象商品を必要な数買った金額を入力
     - **税込み・税抜きのチェック**
       - ここのチェックが間違っていると、**謝礼の過払いや不足が発生します**
       - **間違いの無いよう、確実に判断してください**
       - 大体のレシートには、「小計」と「合計」の2項目は記載されています
         - レシートの「合計」に記載されている金額は、**必ず税込みの金額**になりますので。。。
           - 「小計」と「合計」が全く同じ金額なら、「税込額はチェック」にチェックする
           - 「小計」と「合計」が違う金額なら、「税込額はチェック」にチェックしない
           - 上記で対応お願いします
8. レシート番号
   - この項目は入力不要です
   - 空欄のままで問題ありません

