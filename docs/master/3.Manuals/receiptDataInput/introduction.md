---
permalink: rdi-introduction
category: receipt data input
title: Introduction
---

# Introduction

## ■レシートデータ入力の業務について


ファンくるや提携会社が実施している「お買い物モニター」で提出されてくるレシートを見ながら、  
データを入力するだけの簡単なお仕事です

## ■業務前の準備

以下の環境を用意してください

1. パソコン
    - デスクトップでもノートでもどちらでも構いません
    - セキュリティソフトは必須
        - 常に最新の状態に保ってください
1. WEBブラウザ
    - Google chrome
        - 以下のchrome拡張機能を**必ず**インストールしてください
            - [Receipt Data Input Support Tools](https://chrome.google.com/webstore/detail/receiptdatainputsupportto/lnckinjcigikmigllbfmhbbdnfmcicbj?hl=ja)
1. その他
    1. お茶やジュースなど
        - 必要に応じて
        - **キーボードにこぼさないように注意！！**
    1. 音楽
        - 必要に応じて
    1. 食べ物
        - 必要に応じて
        - **キーボードにこぼさないように注意！！**

## ■マニュアルへのリンクなどなど

1. Chrome拡張機能関連
    - [Receipt Data Input Support Toolsの使い方(未作成)]
    - [Receipt Data Input Wcheck Support Toolsの使い方(未作成)]
1. 業務マニュアル関連
    - [基本入力マニュアル(作成中)](http://masami-nakaoka.dimerapp.com/docs/master/rdi-inputmanual)
    - [Wチェックマニュアル(未作成)]



